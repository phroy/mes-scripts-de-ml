Icons

la_forge-brigit_et_komit.svg :
- Remix by Philippe Roy ( https://forge.apps.education.fr/phroy )
- Initial file : Brigit_et_Komit_transparent.png
- Source repository : https://forge.apps.education.fr/docs/visuel-forge
- Author : Juliette Taka ( https://juliettetaka.com/fr )
- Licence : Creative Commons BY 4.0

la_forge-komit.svg :
- Remix by Philippe Roy ( https://forge.apps.education.fr/phroy )
- Initial file :  avatar_Komit_face_transparent.png 
- Source repository : https://forge.apps.education.fr/docs/visuel-forge
- Author : Juliette Taka ( https://juliettetaka.com/fr )
- Licence : Creative Commons BY 4.0

