# Mes scripts de ML - Introduction aux réseaux de neurones

### Perceptron

![capture d'écran](img/01-perceptron.png)

### Réseaux de neurones avec Keras - Classificateur d'images

![capture d'écran](img/02-keras-classificateur_img.png)

### Réseaux de neurones avec Keras - Classificateur : Points en cercle

![capture d'écran](img/03-keras-tf_playground-cercle.png)

### Réseaux de neurones avec Keras - Classificateur : Ou exclusif (XOR)

![capture d'écran](img/04-keras-tf_playground-xor.png)

### Réseaux de neurones avec Keras - Classificateur : Points sur des gaussiennes

![capture d'écran](img/05-keras-tf_playground-gauss.png)

### Réseaux de neurones avec Keras - Classificateur : Points sur spirales

![capture d'écran](img/06-keras-tf_playground-spiral-v2.png)

### Réseaux de neurones avec Keras - Regression

![capture d'écran](img/07-keras-regression.png)

