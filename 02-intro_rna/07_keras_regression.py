import os, time
import numpy as np
import matplotlib.pyplot as plt

import sklearn
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

import tensorflow as tf
from tensorflow import keras

###############################################################################
# 07_keras_regression.py
# @title: Introduction aux réseaux de neurones - Réseaux de neurones avec Keras - Regression
# @project: Mes scripts de ML
# @lang: fr
# @authors: Philippe Roy <phroy@phroy.org>
# @copyright: Copyright (C) 2023-2024 Philippe Roy
# @license: GNU GPL
###############################################################################

###
# Installation : 
# - pip3 install tensorflow
# - pip3 install keras
# - pip3 install pydot
# - pip3 install graphviz
###

###
# Commandes NumPy :
# - np.array : créer un tableau à partir d'une liste de listes
# - np.c_ : concatène les colonnes des tableaux
# - np.linspace : créer un tableau 1D de la valeur de début à la valeur de fin avec n valeurs
# - np.meshgrid : créer un tableau 2D avec l'ensemble des combinaisons allant des deux valeurs de début aux deux valeurs de fin
# - .reshape : reformater la tableau avec le nombre de lignes et le nombre de colonnes
###

###
# Commandes Scikit-Learn :
# - sklearn.model_selection.train_test_split : partage les données en jeu d'entrainnement et en jeu de test (test_size de 0,25 par défaut)
# - sklearn.preprocessing.StandardScaler() : normalise les données : moyenne nulle et variance unitaire
# - scaler.fit_transform : entrainement et application de la normalisation
# - scaler.transform : application de la normalisation (préentrainée)
###

###
# Commandes Keras :
# - keras.models.Sequential() : créer un modèle où les couches de neurones sont reliées séquentiellement (modèle simple)
# - model.add : ajout d'une couche
# - keras.layers.Flatten : couche de formatage de mise à plat
# - keras.layers.Dense : couche de neurones
# - keras.backend.clear_session() : reset de la session
# - model.compile : compilation du modèle
# - model.fit : entrainement du modèle
# - model.predict : prédiction du modèle
# - keras.utils.plot_model : créer le diagramme d'un modèle
###

###############################################################################
# Initialisation
###############################################################################

# Init du temps
t_debut = time.time()

# Init des plots
fig = plt.figure(figsize=(15, 5))
fig.suptitle("Réseaux de neurones avec Keras - Regression")
model_ax = fig.add_subplot(121) # Modèle
apts_ax = fig.add_subplot(122) # Courbes d'apprentissage
# donnees_ax = fig.add_subplot(133) # Observations : x1,x2 et cibles : y

# Logs
root_logdir = os.path.join(os.curdir, "keras_logs")

def get_run_logdir():
    run_id = time.strftime("run_%Y_%m_%d-%H_%M_%S")
    return os.path.join(root_logdir, run_id)

run_logdir = get_run_logdir()

###############################################################################
# Observations
###############################################################################

# Observations d'apprentissage, de validation et de test
housing = sklearn.datasets.fetch_california_housing() # Jeu de données California housing
X, X_test, y, y_test = sklearn.model_selection.train_test_split(housing.data, housing.target)
X_train, X_valid, y_train, y_valid = train_test_split(X, y)
# X, X_test, y, y_test = sklearn.model_selection.train_test_split(housing.data, housing.target, random_state=42)
# X_train, X_valid, y_train, y_valid = train_test_split(X, y, random_state=42)

# Normalisation
scaler = sklearn.preprocessing.StandardScaler()
X_train = scaler.fit_transform(X_train)
X_valid = scaler.transform(X_valid)
X_test = scaler.transform(X_test)

###############################################################################
# Phase d'apprentissage
###############################################################################

n = 20 # Nombre d'itérations (valeur par défaut : 20 , hyperparamètre)
eta = 0.01 # Taux d'appentissage (valeur par défaut dans Keras : 0.01, hyperparamètre)
lot=32 # Taille de lot (valeur par défaut dans Keras: 32 , hyperparamètre)

perte="mse" # Type de perte (hyperparamètre)
# perte="mean_squared_error"
# perte='mean_absolute_error'
# perte="sparse_categorical_crossentropy"

keras.backend.clear_session()
# np.random.seed(42)
# tf.random.set_seed(42)
model = keras.models.Sequential() # Modèle de réseau de neurones
model.add(keras.layers.Dense(30, input_shape=X_train.shape[1:], activation="relu")) # Couche 1 : 30 nodes
model.add(keras.layers.Dense(1)) # Couche de sortie : 1 node par classe

optimiseur=keras.optimizers.SGD(learning_rate= eta)
model.compile(loss=perte, optimizer=optimiseur) # Compilation du modèle
checkpoint_cb = keras.callbacks.ModelCheckpoint("my_keras_model.h5")
tensorboard_cb = keras.callbacks.TensorBoard(run_logdir)
apts = model.fit(X_train, y_train, epochs=n, batch_size=lot, validation_data=(X_valid, y_valid),  callbacks=[checkpoint_cb, tensorboard_cb]) # Entrainement

###############################################################################
# Phase d'inférence
###############################################################################

X_new = X_test[:3]
y_pred = model.predict(X_new) # Prédiction

###############################################################################
# Résultats
###############################################################################

# Modèle
model_ax.set_title("Modèle")
keras.utils.plot_model(model, "model.png", show_shapes=True)
model_img=plt.imread("model.png")
model_ax.imshow(model_img)
model_ax.set_axis_off()
os.remove("model.png") # Supression du fichier temporaire

# Courbes d'apprentissage
apts_ax.set_title("Courbes d'apprentissage")
apts_ax.plot(apts.epoch, apts.history['loss'], 'b-', label="Perte - entrainement")
apts_ax.plot(apts.epoch, apts.history['val_loss'], 'r-', label="Perte - validation")
apts_ax.set(ylim=(-0.05, 1.05))
apts_ax.set_xlabel("Époque")
apts_ax.legend()

# Plot des données
# FIXME : mettre des graphiques de prédiction
# donnees_ax.set_title("Données")
# plot_i=[]
# plot_x1=[]
# plot_x2=[]
# for i in range (X_valid.shape[0]):
#     plot_i.append(i)
#     plot_x1.append(X_valid[i][0])
#     plot_x2.append(X_valid2[i][0])
# donnees_ax.plot(plot_i, plot_x1)
# donnees_ax.plot(plot_i, plot_x2)
plt.show()

# Performances
print ("Temps total  : "+str(time.time()-t_debut))
