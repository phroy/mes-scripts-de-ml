import time
import numpy as np
import sklearn
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt

###############################################################################
# 01_regression_lineaire.py
# @title: Fondamentaux - Apprentissage par régression linéaire
# @project: Mes scripts de ML
# @lang: fr
# @authors: Philippe Roy <phroy@phroy.org>
# @copyright: Copyright (C) 2023-2024 Philippe Roy
# @license: GNU GPL
###############################################################################

###
# Commandes NumPy :
# - np.array : créer un tableau à partir d'une liste de listes
# - np.c_ : concatène les colonnes des tableaux
# - np.ones : créer un tableau de 1
# - np.linalg.inv : inversion de matrice
# - .T : transposé de matrice
# - .dot : produit de matrice
###

###
# Commandes Scikit-Learn :
# - sklearn.linear_model.LinearRegression() : créer un modèle de régression linéaire (méthode des moindres carrés)
# - .fit : entrainement du modèle
# - .predict : prédiction du modèle
###

###############################################################################
# Initialisation
###############################################################################

# Init du temps
t_debut = time.time()

# Init des plots
fig = plt.figure(figsize=(10, 5))
fig.suptitle("Régression linéaire")
donnees_ax = fig.add_subplot(111)

###############################################################################
# Observations
###############################################################################

# Observations d'apprentisage
m = 1000 # Nombre d'observations
bg = 1 # Quantité du bruit gaussien
x1 = 2*np.random.rand(m, 1) # Liste des observations x1
y = 4 + 3*x1 + bg * np.random.rand(m, 1) # Liste des cibles y
X = np.c_[np.ones((m, 1)), x1] # Matrice des observations, avec x0=1
plt.plot(x1, y, 'b.', label="Observations")

# Nouvelles observations
x1_new=np.array([[0], [2]])
X_new = np.c_[np.ones((2, 1)), x1_new] # Matrice des observations, avec x0=1

###############################################################################
# Phase d'apprentissage
###############################################################################

# Phase d'apprentissage par régression linéaire avec l'équation normale
# - theta : vecteur paramètres du modèle
# - theta_best : vecteur paramètres pour le coût mini
theta_best= np.linalg.inv(X.T.dot(X)).dot(X.T).dot(y)
theta = theta_best

###############################################################################
# Phase d'inférence
###############################################################################

y_predict=X_new.dot(theta_best) # Liste des prédictions y_predict

###############################################################################
# Avec Scikit-Learn (skl)
###############################################################################

model_skl = sklearn.linear_model.LinearRegression() # Modèle régression linéaire
model_skl.fit(x1, y) # Entrainement
# print (model_skl.intercept_[0],model_skl.coef_[0][0])
y_predict_skl=model_skl.predict(x1_new) # Prédiction

###############################################################################
# Résultats
###############################################################################

# Plot des données
donnees_ax.set_title("Données")
donnees_ax.plot(x1_new, y_predict, 'r-', label="Prédictions")
donnees_ax.plot(x1_new, y_predict_skl, 'y-', label="Prédictions - Scikit-Learn")
donnees_ax.set_xlabel(r'$x_1$')
donnees_ax.set_ylabel(r'$y$', rotation=0)
donnees_ax.legend()
plt.show()

# Performances
print ("Theta th    : theta0 : "+str(4)+"     ; theta1 : "+str(3))
print ("Theta skl   : theta0 : "+str(round(float(model_skl.intercept_[0]),3))+" ; theta1 : "+str(round(float(model_skl.coef_[0]),3)))
print ("Theta       : theta0 : "+str(round(float(theta[0]),3))+" ; theta1 : "+str(round(float(theta[1]),3)))
print ("Erreurs     : theta0 : "+str(round(float(theta[0]-4),3))+" ; theta1 : "+str(round(float(theta[1]-3),3)))
print ("Temps       : "+str(time.time()-t_debut))
