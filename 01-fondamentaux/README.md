# Mes scripts de ML - Fondamentaux

### Apprentissage par régression linéaire

![capture d'écran](img/01-regression_lineaire.png)

### Apprentissage par descente de gradient

![capture d'écran](img/02-descente_gradient.png)

### Apprentissage par descente de gradient stochastique

![capture d'écran](img/03-descente_gradient_stochastique.png)

### Apprentissage par descente de gradient par mini-lots

![capture d'écran](img/04-descente_gradient_mini-lots.png)

### Apprentissage par régression polynomiale

![capture d'écran](img/05-regression_polynomiale.png)

### Classificateur par régression logistique

![capture d'écran](img/06-regression_logistique.png)




