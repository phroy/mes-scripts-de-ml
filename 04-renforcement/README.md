# Mes scripts de ML - Apprentissage par renforcement

### Apprentissage par  gradient de politique (PG)

![capture d'écran](img/01-cartpole-gp.png)

### Apprentissage  par décision markoviens (MDP)

![capture d'écran](img/02-cartpole-mdp.png)
	
### Apprentissage  par réseaux de neurones  Q profond (DQN)

![capture d'écran](img/03-cartpole-dqn.png)
