import os, time, random
import numpy as np
import matplotlib.pyplot as plt

import tensorflow as tf
from tensorflow import keras

###############################################################################
# 01_cartpole_gp.py
# @title: Apprentissage par renforcement - Apprentissage par  gradient de politique (PG)
# @project: Mes scripts de ML
# @lang: fr
# @authors: Philippe Roy <phroy@phroy.org>
# @copyright: Copyright (C) 2023-2024 Philippe Roy
# @license: GNU GPL
###############################################################################

###
# Commandes NumPy :
# - np.array : créer un tableau à partir d'une liste de listes
# - np.c_ : concatène les colonnes des tableaux
# - np.linspace : créer un tableau 1D de la valeur de début à la valeur de fin avec n valeurs
# - np.meshgrid : créer un tableau 2D avec l'ensemble des combinaisons allant des deux valeurs de début aux deux valeurs de fin
# - .reshape : reformater la tableau avec le nombre de lignes et le nombre de colonnes
###

###############################################################################
# Initialisation
###############################################################################

# Init du temps
t_debut = time.time()

# Init des plots
fig = plt.figure(layout="constrained", figsize=(20, 7))
fig.suptitle(" Vision par ordinateur - Reconnaissance de digit - Analyse et préparation des données")
img_ax = fig.subplots(10, 40)

###############################################################################
# Observations
###############################################################################

# Observations d'apprentissage, de validation et de test

chiffre = keras.datasets.mnist # Jeu de données MNIST (digit)
# train_filter = np.unique(Y_train, return_index=True)
# X_train, Y_train = X_train[train_filter[1:]], Y_train[train_filter[1:]]
(X, y), (X_test, y_test) = chiffre.load_data()
X_train, y_train  = X[5000:]/255.0 , y[5000:]
X_valid, y_valid = X[:5000]/255.0 , y[:5000]
classes = [0,1,2,3,4,5,6,7,8,9]

###############################################################################
# Affichage des images avec l'étiquette
###############################################################################

print ("\n")
print ("Recherche de 400 images sur le jeu de test (10 000 images).")
# X_new_unique = np.unique(X_test, axis=0, return_index=True)

for ligne in range (10): # Ligne
    i_first=-1
    for colonne in range (40): # Colonne
        for i in range (i_first+1, X_test.shape[0]):
            if y_test[i] == ligne:
            # if y_test[i] == 2:
                img_ax[ligne][colonne].imshow(X_test[i], cmap="binary", interpolation="nearest")
                img_ax[ligne][colonne].set_axis_off()
                img_ax[ligne][colonne].set_title(str(i), fontsize=10)
                i_first=i
                break

print ("\n")

plt.show()
