# Mes scripts de ML

Espace d'essais pour monter en compétence sur le machine learning : Scikit-Learn, TensorFlow, Keras, ...

## Références

Les "manips" sont pour la plupart tirées des livres de Aurélien Géron : 
 - [Deep Learning avec Keras et TensorFlow](https://www.dunod.com/sciences-techniques/deep-learning-avec-keras-et-tensorflow-mise-en-oeuvre-et-cas-concrets-0)
 - [Machine Learning avec Scikit-Learn](https://www.dunod.com/sciences-techniques/machine-learning-avec-scikit-learn-mise-en-oeuvre-et-cas-concrets-1)
 
 Le dépôt associé aux livres : 
 - deuxième édition : https://github.com/ageron/handson-ml2
 - troisième édition : https://github.com/ageron/handson-ml3

### Bibliothèques Python :

 - [NumPy](https://numpy.org) : Manipulation de tableaux, fonctions mathématiques avancées
- [Matplotlib](https://matplotlib.org) : Visualisation de données
- [Scikit-Learn](https://scikit-learn.org) : Apprentissage automatique
- [TensorFlow](https://www.tensorflow.org) : Apprentissage automatique via les réseaux de neurones
- [Keras](https://keras.io/) : Interface de haut niveau pour TensorFlow

### Documentation :

 - [Crash Course de Google Developers](https://developers.google.com/machine-learning/crash-course/ml-intro?hl=fr) : Introduction au machine learning - Google
  - [Fidle](https://gricad-gitlab.univ-grenoble-alpes.fr/talks/fidle/-/wikis/home) :  Fidle (Formation Introduction au Deep Learning) - CNRS, MIAI, UGA
  - [Kaggle](https://www.kaggle.com/) :  Compétitions d'apprentissage automatique
 - [Machine Learnia](https://www.youtube.com/@MachineLearnia) :  Chaîne YouTube sur l'apprentissage automatique
  - [ Hugo Larochelle](https://www.youtube.com/@hugolarochelle/videos) :  Chaîne YouTube sur l'apprentissage automatique
  - [Deepmath](https://exo7math.github.io/deepmath-exo7/) :  Mathématiques des réseaux de neurones

### Hébergement des sources

<table>
  <tr>
    <td> <div> <a href="https://forge.apps.education.fr"> <img src="img/la_forge-brigit_et_komit-blanc.svg" alt="Brigit et Komit"> </a> </div> </td>
   <td> <div> Les fichiers sources de cet espace d'essais sont dans le projet personel <b>Philippe Roy / Mes scripts de ML</b> de La Forge des Communs Numériques
      Éducatifs : https://forge.apps.education.fr/phroy/mes-scripts-de-ml . </div> <div> &nbsp; </div> <div> L'environnement de développement est basé sur le langage
      [Python](https://python.org) et l'éditeur [Spyder](https://www.spyder-ide.org/). </div> </td>
  </tr>
</table>
