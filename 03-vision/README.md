# Mes scripts de ML - Vision par ordinateur

### Reconnaissance de digits - Analyse et préparation des données

![capture d'écran](img/01-digit-prepa_data.png)

### Reconnaissance de digits - Réseaux de neurones simples (PMC)

![capture d'écran](img/02-digit-simple.png)
	
### Reconnaissance de digits - Réseaux de neurones convolutifs (CNN)

![capture d'écran](img/03-digit-cnn.png)

### Visualisation des noyeaux convolutifs (CNN)

![capture d'écran](img/04-cnn-cartes.png)

### Acquisition avec openCV

![capture d'écran](img/05-opencv.png)

