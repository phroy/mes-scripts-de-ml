import os, time, random
import numpy as np
import matplotlib.pyplot as plt

import tensorflow as tf
from tensorflow import keras

###############################################################################
# 02_digit_simple.py
# @title: Vision par ordinateur - Reconnaissance de digit - Réseaux de neurones simples
# @project: Mes scripts de ML
# @lang: fr
# @authors: Philippe Roy <phroy@phroy.org>
# @copyright: Copyright (C) 2023-2024 Philippe Roy
# @license: GNU GPL
###############################################################################

###
# Installation : 
# - pip3 install tensorflow
# - pip3 install keras
# - pip3 install pydot
# - pip3 install graphviz
###

###
# Commandes NumPy :
# - np.array : créer un tableau à partir d'une liste de listes
# - np.c_ : concatène les colonnes des tableaux
# - np.linspace : créer un tableau 1D de la valeur de début à la valeur de fin avec n valeurs
# - np.meshgrid : créer un tableau 2D avec l'ensemble des combinaisons allant des deux valeurs de début aux deux valeurs de fin
# - .reshape : reformater la tableau avec le nombre de lignes et le nombre de colonnes
###

###
# Commandes Keras :
# - keras.models.Sequential() : créer un modèle où les couches de neurones sont reliées séquentiellement (modèle simple)
# - model.add : ajout d'une couche
# - keras.layers.Flatten : couche de formatage de mise à plat
# - keras.layers.Dense : couche de neurones
# - keras.backend.clear_session() : reset de la session
# - model.compile : compilation du modèle
# - model.fit : entrainement du modèle
# - model.predict : prédiction du modèle
# - keras.utils.plot_model : créer le diagramme d'un modèle
###

###############################################################################
# Initialisation
###############################################################################

# Init du temps
t_debut = time.time()

# Init des plots
fig = plt.figure(layout="constrained", figsize=(15, 5))
fig.suptitle("Vision par ordinateur - Reconnaissance de digit par réseaux de neurones simples")
subfigs = fig.subfigures(1, 3)
model_ax = subfigs[0].subplots(1, 1)
apts_ax = subfigs[1].subplots(1, 1)
img_ax = subfigs[2].subplots(10, 15)

###############################################################################
# Observations
###############################################################################

# Observations d'apprentissage, de validation et de test

mnist = keras.datasets.mnist # Jeu de données MNIST (digit)
(X, y), (X_test, y_test) = mnist.load_data()
X_train, y_train  = X[5000:]/255.0 , y[5000:]
X_valid, y_valid = X[:5000]/255.0 , y[:5000]

###############################################################################
# Phase d'apprentissage
###############################################################################

n = 200 # Nombre d'itérations (valeur par défaut : 50 , hyperparamètre)
eta = 0.01 # Taux d'appentissage (valeur par défaut dans Keras : 0.01, hyperparamètre)
lot=32 # Taille de lot (valeur par défaut dans Keras: 32 , hyperparamètre)

perte="sparse_categorical_crossentropy" # Type de perte (hyperparamètre)
#perte="mse" # Type de perte (hyperparamètre)

keras.backend.clear_session()
model = keras.models.Sequential() # Modèle de réseau de neurones
model.add(keras.layers.Flatten(input_shape=[28, 28])) # Couche de mise à plat des données -> 1 node / pixel soit 784 (28x28)
model.add(keras.layers.Dense(300, activation="relu")) # Couche 1 : 300 nodes
model.add(keras.layers.Dense(100, activation="relu")) # Couche 2 : 100 nodes -> ajout
model.add(keras.layers.Dense(10, activation="softmax")) # Couche de sortie : 1 node par classe soit 10

optimiseur=keras.optimizers.SGD(learning_rate= eta)
model.compile(loss=perte, optimizer=optimiseur, metrics=["accuracy"]) # Compilation du modèle

apts = model.fit(X_train, y_train, epochs=n, batch_size=lot, validation_data=(X_valid, y_valid)) # Entrainement

###############################################################################
# Phase d'inférence
###############################################################################

# Inférence sur la totalité du jeu de test
print ("\n")
print ("Test sur le jeu de test (10 000 images).")
X_new = X_test
y_new = np.argmax(model.predict(X_new), axis=-1) # Prédictions
y_new_target= y_test # Cibles
eval=model.evaluate(X_new, y_new_target)
# print ("Il y a "+str(int(np.round((1-eval[1])*X_new.shape[0]))) + " images non reconnues.\n")

# Division du jeu de test par classes
print ("\n")
print ("Test sur les jeux divisés par classe.")
_X_new_classes_lst=[]
_y_new_target_classes_lst=[]
for i in range (10): # Classe
    _X_new_classe=[]
    _y_new_target_classe=[]
    for j in range (X_new.shape[0]): # Lecture de toutes les images
        if y_new_target[j] == i:
            _X_new_classe.append(X_new[j])
            _y_new_target_classe.append(y_new[j])
    _X_new_classes_lst.append(_X_new_classe)
    _y_new_target_classes_lst.append(_y_new_target_classe)

# Remplissage du tableau à partir de la liste
X_new_classes=[]
y_new_target_classes=[]
for i in range (10):
    X_new_classes.append(np.empty(shape=(len(_X_new_classes_lst[i]),28,28)))
    y_new_target_classes.append(np.empty(shape=(len(_y_new_target_classes_lst[i]),)))
    for j in range (len(_X_new_classes_lst[i])):
        X_new_classes[i][[j]]=_X_new_classes_lst[i][j]
        y_new_target_classes[i][[j]]=_y_new_target_classes_lst[i][j]

# Inférence sur les jeux par classe
y_new_classes=[]
for i in range (10):
    y_new_classes.append(np.argmax(model.predict(X_new_classes[i]), axis=-1)) # Prédictions
somme=0
print ("\n")
for i in range (10):
    k=0
    for j in range (X_new_classes[i].shape[0]):
        if y_new_classes[i][j] != i:
            k +=1
            somme +=1
    print ("Dans la classe "+str(i)+", il y a "+str(k) + " images non reconnues sur "+ str(X_new_classes[i].shape[0])+".")

print ("\n")
print ("Au total, il y a "+str(somme) + " images non reconnues sur 10 000.")
print ("Soit une précision de "+str(1-(somme/10000))+".")

# ###############################################################################
# # Résultats
# ###############################################################################

# Modèle
model_ax.set_title("Modèle")
keras.utils.plot_model(model, "model.png", show_shapes=True)
model_img=plt.imread("model.png")
model_ax.imshow(model_img)
model_ax.set_axis_off()
os.remove("model.png") # Supression du fichier temporaire

# Courbes d'apprentissage
apts_ax.set_title("Courbes d'apprentissage")
apts_ax.plot(apts.epoch, apts.history['loss'], 'b-', label="Perte - entrainement")
apts_ax.plot(apts.epoch, apts.history['val_loss'], 'r-', label="Perte - validation")
apts_ax.plot(apts.epoch, apts.history['accuracy'], 'b:', label="Précision - entrainement")
apts_ax.plot(apts.epoch, apts.history['val_accuracy'], 'r:', label="Précision - validation")
apts_ax.set(ylim=(0, 1))
apts_ax.set_xlabel("Époque")
apts_ax.legend()

# Prédictions
for ligne in range (10): # Ligne
    i_first=-1
    for colonne in range (15): # Colonne
        for i in range (i_first+1, X_new.shape[0]):
            img_ax[ligne][colonne].set_axis_off()
            if y_new_target[i] == ligne and y_new[i]!=y_new_target[i]:
            # if y_test[i] == 2:
                img_ax[ligne][colonne].imshow(X_new[i], cmap="binary", interpolation="nearest")
                img_ax[ligne][colonne].set_title(str(y_new[i]), fontsize=10, color="red")
                i_first=i
                break

plt.show()

# Performances
print ("\n")
print ("Temps total  : "+str(time.time()-t_debut))
