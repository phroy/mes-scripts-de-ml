import os, time
import numpy as np
import matplotlib.pyplot as plt

import cv2

###############################################################################
# 05_opencv.py
# @title: Vision par ordinateur - Acquisition vidéo
# @project: Mes scripts de ML
# @lang: fr
# @authors: Philippe Roy <phroy@phroy.org>
# @copyright: Copyright (C) 2023-2024 Philippe Roy
# @license: GNU GPL
###############################################################################

###
# Installation : 
# - pip3 install opencv-python
###

###############################################################################
# Initialisation
###############################################################################

# Init de la caméra
cam_id = 0 # 0 pour la 1ere camera, 1 pour la seconde ...
cam = cv2.VideoCapture(cam_id) # 0 pour la 1ere camera, 1 pour la
assert cam.isOpened(), "Erreur lors de l'ouverture de la camera !"

# Création de la fenêtre d'affichage
cv2.namedWindow("Caméra")

###############################################################################
# Affichage
###############################################################################

# Capture
key=''
while cam.isOpened():
    cam_actif, cam_img = cam.read()
    cv2.imshow("Caméra", cam_img)
    key = cv2.waitKey(1) # Saisie clavier avec un timeout de 1 ms
    if key & 0xFF == ord('q') or key == 27 :
        break
    # cv2.imwrite("camera.png", cam_img)

###############################################################################
# Quitter
###############################################################################

cam.release()
cv2.destroyAllWindows()
